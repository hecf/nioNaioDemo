package com.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * AIO客户端：实现连接的操作
 * @author hcf
 * @email hcf0108@163.com
 * @create 2019-03-08 上午 10:48
 **/
public class AioClient {
    /**  */
    public static final String HOST="127.0.0.1";
    /**  */
    public AsynchronousSocketChannel client = null;


    public AioClient(String host,int port) throws IOException, ExecutionException, InterruptedException {
        client = AsynchronousSocketChannel.open();
        Future<?> future = client.connect(new InetSocketAddress(host,port));
        System.out.println( future.get() );

    }

    public void write(){

    }
}
