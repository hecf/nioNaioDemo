package com.aio;

import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**https://www.cnblogs.com/softidea/p/4575776.html
 * @author hcf
 * @email hcf0108@163.com
 * @create 2019-03-08 上午 11:11
 **/
public class AIOTest {

    @Test
    public void testServer() throws IOException, InterruptedException {
        SimpleServer server = new SimpleServer(7788);

        Thread.sleep(10000);
    }

    @Test
    public void testClient() throws IOException, InterruptedException, ExecutionException {
        SimpleClient client = new SimpleClient("localhost", 7788);
        client.write((byte) 11);
    }

}